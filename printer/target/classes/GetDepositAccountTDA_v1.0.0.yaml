swagger: '2.0'
info:
  version: 1.0.0
  title: Get Deposits Account TDA
  description: >-
    Fetch time deposit account's description.
  
  
    [Test Console](https://srvpocanz001.csc-fsg.com/CeleritiDepositsAPI/#!/GetDepositAccountTDA/getGetDepositAccountTda)
  contact:
    name: Karen Glimme
    url: 'http://www.celeritift.com'
    email: CFTSupportAPIs@celeritift.com
  license:
    name: '© 2017 CeleritiFinTech, an HCL DXC Technology Company. All rights reserved'
    url: 'http://www.celeritift.com'

host: srvpocanz001.csc-fsg.com
tags:
  - name: Account Information
    description: Get account details.
    
schemes:
  - https
produces:
  - application/json
paths:
  '/CeleritiDepositsAPI/services/v1/depositAccountsTda/{accountNbr}':
    get:
      tags: 
       - Account Information
      summary: |
        Fetch time deposit account details.
      description: >
        The Get Deposits Account TDA function retrieves the standard account information, cycle to date aggregate details, current balance and posting information, demographic details, address information and alert information for a time deposit account. The API has the capability to fetch all or some specific information at a time. 
      operationId: getDepositAccountTDA  
      parameters:
        - name: companyNbr
          in: query
          description: Company Number.
          required: false
          type: integer
          format: int32
          minimum: 0
          maximum: 65535
        - name: accountNbr
          in: path
          description: Account Number.
          required: true
          type: integer
          format: int32
          minimum: 0
          maximum: 999999999999999
        - name: accountType
          in: query
          description: 
            Type of account.  
          required: true
          type: string
          format: byte
          maxLength: 3
        - name: extendsFields
          in: query
          description: >-
            Specify the fields that will be included in the partial response.
          required: false
          type: string
          format: byte

      responses:
        '200':
          description: Deposit Account TDA details.
          schema:
            $ref: '#/definitions/depositAccounts'
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/xStatus'
        '404':
          description: Not found.
          schema:
            $ref: '#/definitions/xStatus'
        '422':
          description: 
            Unprocessable entity. Will be used in case of business and
            validation error.
          schema:
            $ref: '#/definitions/xStatus'
        '500':
          description: Internal server error.
          schema:
            $ref: '#/definitions/xStatus'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/xStatus'
definitions:
  depositAccounts:
    type: object
    properties:
      xStatus:
        $ref: '#/definitions/xStatus'
      standardAccountInfo:
        type: object
        description: Original Account Information 
        properties:
          accountType:
            type: string
            format: byte
            description: Type of the account. 
            maxLength: 3
          product:
            type: string
            format: byte
            description: An account is linked to a Product type. Combination of AccountType and
              Product is used to default majority of account control information
              during account setup. It is defined as 2 character code. 
            maxLength: 2
          branch:
            type: integer
            format: int32
            description: 
              Branch defined for the account. 
            minimum: 0
            maximum: 99999
          customerProfitGroupCode:
            type: string
            format: byte
            description: Client defined code for indicating the profitability group to be assigned
              to the account.The value will be defaulted based on subproduct code or account plan.
            maxLength: 2
          packageCode:
            type: string
            format: byte
            description: Package code under which the account will be opened. Package code may
              be used by the marketing department to identify the advertising
              campaign. It is defaulted based on the combination of values provided
              in AccountType and Product.
            maxLength: 6
          accountStatus:
            type: string
            format: byte
            description: Describes the current status of the account. 
            maxLength: 2  
          transactionAdviceCode:
            type: string
            format: byte
            description: Describes debit and credit notice for this account.
             
            maxLength: 1
          obpCode:
            type: string
            format: byte
            description: Out of bound processing code controls the balance used for paying
              normal debits against the account and determines what is to be done
              when there are insufficient funds to pay an item.
            maxLength: 2
          accountRestClass1:
            type: string
            format: byte
            description:  Describes whether on the account there is a Class 1 Hard Hold 
              restraint.
            maxLength: 1
          accountRestClass2:
            type: string
            format: byte
            description:  Describes whether on the account there is a Class 2 
              restraint i.e. pledge. 
            maxLength: 1
          accountRestClass3:
            type: string
            format: byte
            description: Describes whether on the account there is a Class 3 
              restraint i.e. Stop Pay. 
            maxLength: 1 
          svcChgCode:
            type: string
            format: byte
            description: Service charge code. Service charge method to be used in
              calculating the service charge on the account.
            maxLength: 2
          svcChgWaiveCode:
            type: string
            format: byte
            description: Service charge waive code. This defines the service charge waive
              criteria. 
            maxLength: 2
          IntCode:
            type: string
            format: byte
            description: Debit interest code.It defines the method to be used for
              calculating debit interest on a demand deposit account.
            maxLength: 2
          IntWaiveCode:
            type: string
            format: byte
            description: Debit interest waive code. This indicates the reason for waiving
              debit interest assessed on an account.
            maxLength: 2
          lateChargeCode:
            type: string
            format: byte
            description: Late charge code assigned to the account
            maxLength: 2
          lateChargeWaiveCode:
            type: string
            format: byte
            description: Late charge waive code describes the reason for waiving a late charge.
            maxLength: 2
          arpCode:
            type: string
            format: byte
            description: Account reconciliation to be run for the account. 
             
            maxLength: 1
          zeroBalanceAccountCode:
            type: string
            format: byte
            description: This field indicates whether the account is part of a zero balance Driver 
              Processing relationship or not. The field is set by the system when a Target 
              Balance ZBA driver is added. 
            maxLength: 1
          summaryPostCode:
            type: string
            format: byte
            description: This is a client-defined code that indicates whether this account uses the 
              summary post function or not. 
            maxLength: 1
          driverProcessing:
            type: string
            format: byte
            description:  This field indicates driver Processing relationship. 
            maxLength: 1  
          odLineAmount:
            type: number
            format: float
            description: The amount of overdraft limit allowed for the acccount.
            minimum: -99999999999.99
            maximum: 99999999999.99
          significantBalanceChangeCode:
            type: string
            format: byte
            description: This field is used to determine whether the account should be reported on
              Significant Balance Change Report.
            maxLength: 2  
          largeItemMonitor:
            type: string
            format: byte
            description: Large item monitor code is used to monitor and report large monetary
              items posted against the account. 
            maxLength: 1
          portfolioAccountType:
            type: string
            format: byte
            description: Indicates whether this account is the primary account or a secondary account in the portfolio.
            maxLength: 1        
            
          newRegDCategory:
            type: string
            format: byte
            description: Used to change the current Regulation D category of the account. 
            maxLength: 1   
          currentRegDCategory:
            type: string
            format: byte
            description: Regulation D category assigned to the account. 
            maxLength: 1                         
          currency:
            type: string
            format: byte
            description: Currency assigned to the account
            maxLength: 3            
          withholdingHistoryFlag:
            type: string
            format: byte
            description: Indicates whether withholding history exists on the account. This field is updated by the system when withholding is calculated and assessed during an interest cycle. 
          language:
            type: string
            format: byte
            description: Language for the account.
            maxLength: 3   
          bonusInterestPresent:
            type: string
            format: byte
            description: Indicates whether bonus interest currently applies to this account.
            maxLength: 1        
          taxExempt1:
            type: string
            format: byte
            description: Tax exemption type 1.
            maxLength: 1
          taxExempt2:
            type: string
            format: byte
            description: Tax exemption type 2.
            maxLength: 1
          taxExempt3:
            type: string
            format: byte
            description: Tax exemption type 3.
            maxLength: 1  
          restraintNoticeIndicator:
            type: string
            format: byte
            description: >-
              This flag indicates whether Stop Payment notices should be produced for
              the account or not.
            maxLength: 1
          rejectTransactionSet:
            type: string
            format: byte
            description: >-
              This is used to identify sets of transaction codes on the Specific Transaction Code Reject parameter set.
            maxLength: 5
          processingId:
            type: integer
            format: int32
            description: >-
              Processing Identifier is the value defined by the institutions to assign accounts to processing groups.
            minimum: 0
            maximum: 99999    
      cycleToDateDetails:
        description: Cycle to date aggregates for the account.
        type: object
        properties:
          lastCycleDate:
            type: string
            format: date yyyy-mm-dd
            description: >-
              Date of the last interest cycle.            
          balancesLastAggDate:
            type: string
            format: date yyyy-mm-dd
            description: >-
              This date is on which the year-to-date balances were last aggregated 
              for the account.
          aggPlusLedgerBalance:
            type: number
            format: float
            description: >-
              The amount is aggregate plus (positive) ledger balance for the
              current debit interest cycle in progress. This field is initialized to zeros
              when the debit interest cycle is completed.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          aggPlusLedgerDays:
            type: integer
            format: int32
            description: >-
              This indicates aggregate number of calendar days the account had a positive 
              ledger balance during the current debit interest cycle in progress. This field
              is updated each processing day as appropriate based on the ledger balance of 
              the account. This field is initialized to zeros when the debit interest cycle is
              completed.
            minimum: 0
            maximum: 999
          aggOverdrawnLedgerBalance:
            type: number
            format: float
            description: >-
              The amount is aggregate overdrawn (negative) ledger balance for the current
              debit interest cycle in progress. This field is updated each processing day 
              as appropriate based on the ledger balance of the account. This field is 
              initialized to zeros when the debit interest cycle is completed.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          aggOverdrawnLedgerDays:
            type: integer
            format: int32
            description: >-
              This indicates aggregate number of calendar days the account had a negative
              ledger balance during the current debit interest cycle in progress. This field
              is updated each processing day as appropriate based on the ledger balance of 
              the account. This field is initialized to zeros when the debit interest cycle is
              completed.
            minimum: 0
            maximum: 999
          aggPlusCollectedBalance:
            type: number
            format: float
            description: >-
              The amount is aggregate plus (positive) collected balance for the current
              debit interest cycle in progress. This field is updated each processing day 
              as appropriate based on the collected balance of the account. This field is 
              initialized to zeros when the debit interest cycle is completed.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          aggPlusCollectedDays:
            type: integer
            format: int32
            description: >-
              This indicates aggregate number of calendar days the account had a positive
              collected balance during the current debit interest cycle in progress. 
              This field is updated each processing day as appropriate based on the collected
              balance of the account. This field is initialized to zeros when the debit
              interest cycle is completed.
            minimum: 0
            maximum: 999
          aggOverdrawnCollectedBalance:
            type: number
            format: float
            description: >-
              The amount is aggregate negative collected balance for the current
              debit interest cycle in progress. This field is updated each processing day 
              as appropriate based on the collected balance of the account. This field is 
              initialized to zeros when the debit interest cycle is completed.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          aggOverdrawnCollectedDays:
            type: integer
            format: int32
            description: >-
              This indicates aggregate number of calendar days the account had a negative
              collected balance during the current debit interest cycle in progress. 
              This field is updated each processing day as appropriate based on the collected
              balance of the account. This field is initialized to zeros when the debit
              interest cycle is completed.
            minimum: 0
            maximum: 999
          ledgerBalanceLow:
            type: number
            format: float
            description: >-
              Lowest overdrawn ending balance (smallest overdraft balance) in the account
              during the current debit interest cycle in progress. This field is 
              initialized to zeros when the debit interest cycle is completed.
            minimum: -99999999999.99
            maximum: 99999999999.99
          ledgerBalanceHigh:
            type: number
            format: float
            description: >-
              Highest overdrawn ending balance (largest overdraft balance) in the account
              during the current debit interest cycle in progress. This field is 
              initialized to zeros when the debit interest cycle is completed.
            minimum: -99999999999.99
            maximum: 99999999999.99
          collectedBalanceLow:
            type: number
            format: float
            description: >-
              Lowest overdrawn ending collected balance (smallest overdraft collected
              balance) in the account during the current debit interest cycle in progress.
              This field is initialized to zeros when the debit interest cycle is completed.
            minimum: -99999999999.99
            maximum: 99999999999.99
          collectedBalanceHigh:
            type: number
            format: float
            description: >-
              Highest overdrawn ending collected balance (largest overdraft collected
              balance) in the account during the current debit interest cycle in progress.
              This field is initialized to zeros when the debit interest cycle is completed.
            minimum: -99999999999.99
            maximum: 99999999999.99
          creditsCount:
            type: integer
            format: int32
            description: >-
              This indicates number of credits posted to the account during current cycle.
            minimum: 0
            maximum: 9999999
          aggAmountCredits:
            type: number
            format: float
            description: >-
              The amount is aggregate of all the credits posted to account.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          debitsCount:
            type: integer
            format: int32
            description: >-
              This indicates number of debits posted to the account during current cycle.
            minimum: 0
            maximum: 9999999
          aggAmountDebits:
            type: number
            format: float
            description: >-
              The amount is aggregate of all the debits posted to account.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          ChargeableCreditsCount:
            type: integer
            format: int32
            description: >-
              Aggregate number of chargeable credits posted for the current interest cycle in progress.
            minimum: 0
            maximum: 9999999
          ChargeableDebitsCount:
            type: integer
            format: int32
            description: >-
              Aggregate number of chargeable debits posted for the current interest cycle in progress.
            minimum: 0
            maximum: 9999999                
          paperCreditsCount:
            type: integer
            format: int32
            description: >-
              This number indicates aggregate count of all paper credits posted to
              the account.
            minimum: 0
            maximum: 9999999
          paperDebitsCount:
            type: integer
            format: int32
            description: >-
              This number indicates aggregate count of all paper debits posted to
              the account.
            minimum: 0
            maximum: 9999999
          paperlessCreditsCount:
            type: integer
            format: int32
            description: >-
              This number indicates aggregate count of all paperless credits posted
              to the account.
            minimum: 0
            maximum: 9999999
          paperlessdebitsCount:
            type: integer
            format: int32
            description: >-
              This number indicates aggregate count of all paperless debits posted
              to the account.
            minimum: 0
            maximum: 9999999
          chargebackCount:
            type: integer
            format: int32
            description: >-
              Aggregate number of charge back items posted for the current interest cycle in progress
            minimum: 0
            maximum: 9999999
          chargebackAmount:
            type: number
            format: float
            description: Aggregate amount of all charge back items posted to the account for the current interest cycle in progress
            minimum: -9999999999999.99
            maximum: 9999999999999.99  
          depositsCount:
            type: integer
            format: int32
            description: Aggregate number of credits posted to the account that are considered deposits for the current interest cycle in progress.
            minimum: 0
            maximum: 9999999
          depositsAmount:
            type: number
            format: float
            description: Aggregate amount of all credits posted to the account that are considered deposits for the current interest cycle in progress.
            minimum: -9999999999999.99
            maximum: 9999999999999.99 
          depositedItemsCount:
            type: integer
            format: int32
            description: Aggregate number of local deposited items for the current interest cycle in progress.
            minimum: 0
            maximum: 9999999        
           
          withdrawalsCount:
            type: integer
            format: int32
            description: This number indicates aggregate count of all paperless debits posted
              to the account.
            minimum: 0
            maximum: 9999999
          withdrawalsAmount:
            type: number
            format: float
            description: The amount is aggregate of all the debits posted to account.
            minimum: -9999999999999.99
            maximum: 9999999999999.99
          balanceAfterInterestPayment:
            type: number
            format: float
            description: Balance of the account after the interest payment was posted during the last interest payment cycle.
            minimum: -9999999999999.99
            maximum: 9999999999999.99     
          interestPenaltyCtd:
            type: number
            format: float
            description: Aggregate amount of interest penalty assessed on the account current interest cycle-to-date.
            minimum: -9999999999999.99
            maximum: 9999999999999.99                
        
          interestLastAccruedDate:
            type: string
            format: date yyyy-mm-dd
            description: Interest is accrued to this date.
            
          interestLastAccrued:
            type: number
            format: float
            description: Amount of interest last accrued for this account.
            minimum: -9999999.99999999
            maximum: 9999999.99999999
          interestAccruedCtd:
            type: number
            format: float
            description: Amount of interest accrued for this account current interest cycle-to-date.
            minimum: -9999999.99999999
            maximum: 9999999.99999999
          interestPaidCtd:
            type: number
            format: float
            description: Amount of the last interest payment.
            minimum: -9999999.99
            maximum: 9999999.99
          balanceForInterestAccrual:
            type: number
            format: float
            description: Balance in the account that is subject to interest accruals.
            minimum: -99999999999.99
            maximum:  99999999999.99            
          priorAverageBalanceCtd:
            type: number
            format: float
            description: Cycle-to-date average ledger balance from the prior interest cycle. This field is updated during the interest payment cycle process.
            minimum: -99999999999.99
            maximum:  99999999999.99 
          priorMinimumBalanceCtd:
            type: number
            format: float
            description: Minimum (low) ledger balance cycle-to-date from the prior interest cycle. This field is updated during the interest payment cycle process
            minimum: -99999999999.99
            maximum:  99999999999.99            
      balancePostingInfo:
        type: object
        description: Balance and posting information of the account
        properties:
          currentBalance:
            type: number
            format: float
            description: Current ledger balance in the account.
            minimum: -99999999999.99
            maximum: 99999999999.99
          currentCollectedBalance:
            type: number
            format: float
            description: Current collected balance in the account.
            minimum: -99999999999.99
            maximum: 99999999999.99
          pledgedRestraintsAmt:
            type: number
            format: float
            description: Total amount of funds held due to all Class 2 (pledged) restraints.
            minimum: -99999999999.99
            maximum: 99999999999.99
          lastCustomerActivityDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last activity that is considered customer contact.
          lastTransactionDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Date of the last transaction that was posted against the account.
          lastTransactionCode:
            type: string
            format: byte
            description: Transaction code of the last transaction that was posted to the account.
            maxLength: 4     
          lastDrCrDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last debit or credit transaction that was posted against this account.
          lastDrCrTransactionCode:
            type: string
            format: byte
            description: Transaction code of the last debit or credit transaction that was posted against this account.
            maxLength: 4  
          lastDrCrTransactionAmt:
            type: number
            format: float
            description: Amount of the last debit or credit transaction that was posted against the account.
            minimum: -99999999999.99
            maximum: 99999999999.99  
          lastMaintenanceDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Date on which the last file maintenance transaction was processed on this account.
          lastDepositDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last deposit that was posted against this account.
          lastDepositAmt:
            type: number
            format: float
            description: Amount of the last deposit transaction posted against this account.
            minimum: -99999999999.99
            maximum: 99999999999.99
          lastDirectDepositDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last transaction that is considered a direct deposit that was posted against this account
          lastDirectDepositAmt:
            type: number
            format: float
            description: Amount of the last transaction posted against this account that is considered a direct deposit.
            minimum: -99999999999.99
            maximum: 99999999999.99  
          lastWithdrawalDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last withdrawal that was posted against this account.
          lastWithdrawalAmt:
            type: number
            format: float
            description: Amount of the last withdrawal transaction posted against this account.
            minimum: -99999999999.99
            maximum: 99999999999.99  
          lastDirectWithdrawalDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Effective date of the last transaction that is considered a direct withdrawal that was posted against this account.
          lastDirectWithdrawalAmt:
            type: number
            format: float
            description: Amount of the last transaction posted against this account that is considered a direct withdrawal.
            minimum: -99999999999.99
            maximum: 99999999999.99              
          lastCustomerContactDate:
            type: string
            format: date (yyyy-mm-dd)
            description: Date of the last verbal or written communication with the customer or specific file maintenance.  
      demographicDetails:
        type: object
        properties:
          shortName:
            type: string
            format: byte
            description: Customer short name assigned to the account
            maxLength: 25
          fundsOwnerCode:
            type: string
            format: byte
            description: Funds ownership. The value in this field is part of the criteria used to establish the Reg D category for the account.
            maxLength: 2
          fundsSubOwnerCode:
            type: string
            format: byte
            description: Funds subowner code assigned to the account. Subowner is a finer breakdown of the ownership of the funds in the account.
            maxLength: 2                 
          costCentre:
            type: integer
            format: int32
            description: cost center assigned to the account
            minimum: 0
            maximum: 9999999
          relOfficer1:
            type: string
            format: byte
            description: Primary officer assigned to the account.
            maxLength: 5
          relOfficer2:
            type: string
            format: byte
            description: Secondary officer assigned to the account.
            maxLength: 5   
          sic:
            type: string
            format: byte
            description:  SIC code
            maxLength: 4
          geographicCode:
            type: string
            format: byte
            description: code that indicates the geographic location of the account.
            maxLength: 4
          location:
            type: string
            format: byte
            description: code that indicates the location of the account.
            maxLength: 5   
          podBank:
            type: string
            format: byte
            description: Indicates whether float amounts and float days are sent with the credit transactions to the system.
            maxLength: 1 
          floatFactor:
            type: number
            format: float
            description: Factor to be used in this account in calculating float if the financial institutions is not sending actual float amounts on the credit transactions.
            minimum: -9.99
            maximum: 9.99
          freeCheckCount:
            type: integer
            format: int32
            description: Number of free checks ditributed to the customer at no cost, when the account was opened
            minimum: 0
            maximum: 999999999
          abandonedFundsCode:
            type: string
            format: byte
            description: AFC that is assigned to the account.
            maxLength: 4
          naics:
            type: string
            format: byte
            description: NAICS. North American Industry Classification System code.
            maxLength: 6   
 
      accountAddressInfo:
        type: object
        properties:
          nameLine1:
            type: string
            format: byte
            description: Name line 1 of the account title.
            maxLength: 40     
          nameLine2:
            type: string
            format: byte
            description:  Name line 2 of the account title.
            maxLength: 40
          comment1:
            type: string
            format: byte
            description: Name & Address Comment 1 of the account title.
            maxLength: 40     
          comment2:
            type: string
            format: byte
            description:  Name & Address Comment 2 of the account title.
            maxLength: 40
          addressLine1:
            type: string
            format: byte
            description: Address line 1 for the accountholder
            maxLength: 40 
          addressLine2:
            type: string
            format: byte
            description:  Address line 2 for the accountholder
            maxLength: 40  
          defaultCityState:
            type: string
            format: byte
            description: Indicates whether city or state is to be supplied from the ZIP code table. 
            maxLength: 1  
          city:
            type: string
            format: byte
            description: City name for the accountholder
            maxLength: 25      
          state:
            type: string
            format: byte
            description: State name for the accountholder
            maxLength: 15 
          postalCode:
            type: integer
            format: int32
            description: Postal or zip code for the accountholder
            minimum: 0
            maximum: 99999
          postalCodeAlphaNum:
            type: integer
            format: int32
            description: Alpha-numeric Postal or zip code for the accountholder.
            minimum: 0
            maximum: 999999999
          ssnTaxpayerId:
            type: integer
            format: int32
            description: Social security number or tax payer identification number of the accountholder
            minimum: 0
            maximum: 9999999999999999 
      alertInfo:
        type: object
        description: Alert information (repeat 15 times)
        items:
          properties:
            alertType:
              type: string
              format: byte
              description: Type of alert
              maxLength: 4    
            alertCode:
              type: string
              format: byte
              description: Code value of the alert type
              maxLength: 4    
            alertExpireDate:
              type: string
              format: date (yyyy-mm-dd)
              description: Expiration date of the alert                    
                          
  xStatus:
    type: object
    properties:
      statusmessage:
        type: string
        format: byte
        description: 
          Status message. User readable message describing the status or error
          condition. This will display business message (Business message may be
          a successful or an error message in case it is coming from mainframe).
          If there is an OpenLegacy validation error, then “Invalid Error” would
          appear in this field.
        maxLength: 40
      severity:
        type: string
        format: byte
        description: 
          Severity of the Status message. Severity will be defined as
          I-Information, E-error, W- warning and S-Severe.
        maxLength: 1
      statusCd:
        type: integer
        format: int32
        description: Status Code. Condition code assigned to the status message.
        minimum: 0
        maximum: 65535
      applicationCd:
        type: integer
        format: int32
        description: Application Code.
        minimum: 0
        maximum: 65535
      detailMessages:
        type: array
        items:
          $ref: '#/definitions/detailMessage'
  detailMessage:
    type: object
    properties:
      propertyName:
        type: string
        format: byte
        description: 
          Identifies the property of the object that the error is related to.
          This is defined for validation errors.
        maxLength: 40
      messageDescription:
        type: string
        format: byte
        description: Message description. This will include validation error messages for
          the corresponding property.
        maxLength: 100
                        