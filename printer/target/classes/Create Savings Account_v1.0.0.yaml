swagger: '2.0'
info:
  version: 1.0.0
  title: Create Savings Account
  description: Create a saving account. 
  
    
    [Test Console](https://srvpocanz001.csc-fsg.com/CeleritiDepositsAPI/#!/SavingAccount/getSavingAccount)
  contact:
    name: Karen Glimme
    url: 'http://www.celeritift.com'
    email: CFTSupportAPIs@celeritift.com
  license:
    name: © 2017 CeleritiFinTech, an HCL DXC Technology Company. All rights reserved
    url: 'http://celeritift.com/'
host: srvpocanz001.csc-fsg.com
tags:
  - name: Create Accounts
    description: 'Create Card, checking, and savings account.'
schemes:
  - https
produces:
  - application/json
paths:
  /CeleritiDepositsAPI/services/v1/savingsAccounts:
    post:
      tags: 
       - Create Accounts
      summary: Create a saving account.
      description: >-
        This API is used for setting up saving account. The user can select a
        product type from the supported options and create a saving account. A
        saving account is an interest bearing deposit account held with a
        financial institution and provides a moderate interest rate. A saving
        account can also be a term deposit with a specified period of maturity
        and earns interest.
      operationId: CreateSavingsAccount
      parameters:
        - name: savingAccount
          in: body
          description: Account details for setting up a saving account.
          schema:
            $ref: '#/definitions/savingAccount'
          required: true
      responses:
        '200':
          description: Demand deposit account details.
          schema:
            $ref: '#/definitions/response'
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/xStatus'
        '404':
          description: Not found.
          schema:
            $ref: '#/definitions/xStatus'
        '422':
          description: >-
            Unprocessable entity. Will be used in case of business and
            validation error.
          schema:
            $ref: '#/definitions/xStatus'
        '500':
          description: Internal server error.
          schema:
            $ref: '#/definitions/xStatus'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/xStatus'
definitions:
  response:
    type: object
    properties:
      xStatus:
        $ref: '#/definitions/xStatus'
      savingAccount:
        $ref: '#/definitions/savingAccount'
      accountStatus:
        type: string
        format: byte
        description: >-
          Describes the current status of the account.
        maxLength: 2
  savingAccount:
    type: object
    required:
      - accountNumber
      - accountType
      - product
    properties:
      companyNbr:
        type: integer
        format: int32
        description: Company Number.
        minimum: 0
        maximum: 65535
      accountNumber:
        type: integer
        format: int32
        description: A unique number given to an individual account in a bank.
        minimum: 0
        maximum: 999999999999999
      accountType:
        type: string
        format: byte
        description: >-
          Type of the account. 
        maxLength: 3
      accountName:
        type: string
        format: byte
        description: Customer Name assigned to the account.
        maxLength: 25
      product:
        type: string
        format: byte
        description: >-
          An account is linked to a Product type. Combination of AccountType and
          Product is used to default majority of account control information
          during account setup. It is defined as 2 character code. 
        maxLength: 2
      processingId:
        type: integer
        format: int32
        description: >-
          Processing Identifier to assign accounts to processing groups. Valid
          value:- 00000
        minimum: 0
        maximum: 99999
      openDate:
        type: string
        format: date yyyy-mm-dd
        description: >-
          Account opening date. It defaults to the current system date if
          nothing is defaulted or provided by the user.
      birthDate:
        type: string
        format: date yyyy-mm-dd
        description: >-
          Account holder birth date. This field is required for Retirement
          account (REA) AccountType.
      retirementPlan:
        type: string
        format: byte
        description: >-
          Retirement plan for the account. This contains the can and must
          withdraw ages, contributions, rollover limits and other retirement
          control criteria. These are defined as 2 character code. This field is
          only applicable for account type REA.
        maxLength: 2
      packageCode:
        type: string
        format: byte
        description: >-
          Package code under which the account will be opened. Package code may
          be used by the marketing department to identify the advertising
          campaign. It is defaulted based on the combination of values provided
          in AccountType and Product.
        maxLength: 6
      obpCode:
        type: string
        format: byte
        description: >-
          Out of bound processing code controls the balance used for paying
          normal debits against the account and determines what is to be done
          when there are insufficient funds to pay an item. This field is
          applicable for RSV account type only. This field will be part of
          response only. 
        maxLength: 2
      socialSecurityNumber:
        type: integer
        format: int32
        description: >-
          Social Security number. User can provide either one of
          SocialSecurityNumber or TaxIdNumber.
        minimum: 0
        maximum: 999999999
      taxIdNumber:
        type: integer
        format: int32
        description: >-
          Tax Identification number. User can provide either one of
          SocialSecurityNumber or TaxIdNumber.
        minimum: 0
        maximum: 999999999
      language:
        type: string
        format: byte
        description: Language of the account holder. 
        maxLength: 3
      withholdCode:
        type: string
        format: byte
        description: >-
          Withholding code assigned to the account. These are defined as 2
          character code. 
        maxLength: 2
      largeItemMonitor:
        type: string
        format: byte
        description: >-
          Large item monitor code is used to monitor and report large monetary
          items posted against the account. A value of zero is valid for CDA,
          CSV, REA, RSV and TOA accounts.This field will be part of response
          only.
        maxLength: 1
      currency:
        type: string
        format: byte
        description: >-
          Currency designated for the account. 
        maxLength: 3
      demographicDetails:
        description: Demographic details for the account.
        type: object
        required:
          - ownerCode
        properties:
          branch:
            type: integer
            format: int32
            description: >-
              Branch defined for the account. 
            minimum: 0
            maximum: 99999
          costCenter:
            type: integer
            format: int32
            description: >-
              Cost Center for the account. 
            minimum: 0
            maximum: 9999999
          relOfficer1:
            type: string
            format: byte
            description: >-
              Primary officer assigned to the account.
            maxLength: 5
          relOfficer2:
            type: string
            format: byte
            description: >-
              Secondary officer assigned to the account.
            maxLength: 5
          ownerCode:
            type: string
            format: byte
            description: >-
              Funds Ownership code. The value in this field is part of the
              criteria used to establish the RegD category for the account.
             
            maxLength: 2
          subownerCode:
            type: string
            format: byte
            description: >-
              Code that provides a further breakdown of the ownership of the
              funds in the account. 
            maxLength: 2
          location:
            type: string
            format: byte
            description: Location of the account.
            maxLength: 2
          geographicCode:
            type: string
            format: byte
            description: >-
              Geographic location of the account. 
             
            maxLength: 4
          sic:
            type: string
            format: byte
            description: >-
              Standard industrial classification or occupation code.
            maxLength: 4
          naics:
            type: string
            format: byte
            description: >-
              North American Industry Classification System code. 
            maxLength: 6
      creditInterest:
        description: >-
          This object defines the Credit interest criteria for an account. These
          can be defined by the user or are defaulted during account setup.
        type: object
        properties:
          intCode:
            type: string
            format: byte
            description: >-
              Interest code. It defines the method to be used for calculating
              credit interest on a term deposit account.
            maxLength: 2
          intWaiveCode:
            type: string
            format: byte
            description: >-
              Interest waive code. This indicates the reason for waiving credit
              interest. 
            maxLength: 2
          intCycle:
            type: string
            format: byte
            description: >-
              Interest cycle. Indicates when the account is to be cycled for
              interest payment. 
            maxLength: 2
          intPaymentMethod:
            type: string
            format: byte
            description: >-
              Interest payment method. Method for payment of interest to the
              account. It defines if the payment is by check to owner, check to
              third party, transfer to this account etc.
            maxLength: 2
      serviceCharge:
        description: >-
          This object defines the service charge definitions for an account.
          These can be defined by the user or are defaulted during account
          setup.
        type: object
        properties:
          svcChgCode:
            type: string
            format: byte
            description: >-
              Service charge code. Service charge method to be used in
              calculating the service charge on the account. 
            maxLength: 2
          svcChgWaiveCode:
            type: string
            format: byte
            description: >-
              Service charge waive code. This defines the service charge waive
              criteria.This field is not applicable for account type CDA and
              TOA. 
            maxLength: 2
          svcChgCycle:
            type: string
            format: byte
            description: >-
              Service charge cycle. Cycle when the service charge is to be
              assessed. This field is not applicable for account type CDA, CSV
              and TOA.
            maxLength: 2
      statement:
        description: >-
          Statement object defines the statement cycle, statement mail and
          sequence of items in statement for an account. These can be defined by
          the user or are defaulted during account setup.
        type: object
        properties:
          statementCycle:
            type: string
            format: byte
            description: >-
              Statement cycle. Code that defined when the Statement is to cycle.
             
            maxLength: 2
          statementMailCode:
            type: string
            format: byte
            description: >-
              Statement mail code. Mail code assigned to the account. It defines
              whether it will be a regular mail to the account holder or there
              is some special handling required. 
            maxLength: 2
          statementSequence:
            type: string
            format: byte
            description: >-
              Statement detail sequence. It defines the sequence of items in the
              statement. It can be in a chronological order or based on serial
              number or both.
            maxLength: 2
      termDeposit:
        description: >-
          This object defines the basic parameters associated with the term
          deposits. These can be defined by the user or are defaulted on the
          basis of product defined by the user during account setup.
        type: object
        properties:
          term:
            type: integer
            format: int32
            description: >-
              It indicates the term amount of the deposit. Values are in days,
              months or years depending on the Term code. This field is
              applicable for account type CDA and REA.
            minimum: 0
            maximum: 99999
          termCode:
            type: string
            format: byte
            description: >-
              It indicates the term period of the deposit. This field is
              applicable for account type CDA and REA.
            maxLength: 1
          issueDate:
            type: string
            format: date yyyy-mm-dd
            description: >-
              Issue date of the term deposit. Open date is defaulted in this
              field if this field is left blank.
          maturityDate:
            type: string
            format: date yyyy-mm-dd
            description: >-
              Current maturity date of the term deposit. If Term and termCode
              are not defined, the date defined in this field enables the
              calculation of Term and TermCode based on the value defined here
              and IssueDate.
  xStatus:
    type: object
    properties:
      statusmessage:
        type: string
        format: byte
        description: >-
          Status message. User readable message describing the status or error
          condition. This will display business message (Business message may be
          a successful or an error message in case it is coming from mainframe).
          If there is an OpenLegacy validation error, then “Invalid Error” would
          appear in this field.
        maxLength: 40
      severity:
        type: string
        format: byte
        description: >-
          Severity of the Status message. Severity will be defined as
          I-Information, E-error, W- warning and S-Severe.
        maxLength: 1
      statusCd:
        type: integer
        format: int32
        description: Status Code. Condition code assigned to the status message.
        minimum: 0
        maximum: 65535
      applicationCd:
        type: integer
        format: int32
        description: Application Code.
        minimum: 0
        maximum: 65535
      detailMessages:
        type: array
        items:
          $ref: '#/definitions/detailMessage'
  detailMessage:
    type: object
    properties:
      propertyName:
        type: string
        format: byte
        description: >-
          Identifies the property of the object that the error is related to.
          This is defined for validation errors.
        maxLength: 40
      messageDescription:
        type: string
        format: byte
        description: >-
          Message description. This will include validation error messages for
          the corresponding property.
        maxLength: 100
          