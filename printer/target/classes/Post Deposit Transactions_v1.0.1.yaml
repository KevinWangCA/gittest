swagger: '2.0'
info:
  version: 1.0.1
  title: Post Deposit Transactions
  description: Post a transaction to a checking or saving account. 
  
    
    [Test Console](https://srvpocanz001.csc-fsg.com/CeleritiDepositsAPI/#!/AccountTransactions/getPostTransactions)
  contact:
    name: Karen Glimme
    url: 'http://www.celeritift.com'
    email: CFTSupportAPIs@celeritift.com
  license:
    name: © 2017 CeleritiFinTech, an HCL DXC Technology Company. All rights reserved
    url: 'http://celeritift.com/'
host: srvpocanz001.csc-fsg.com
tags:
  - name: Payments
    description: Post loan and deposit account transactions.
schemes:
  - https
produces:
  - application/json
paths:
  '/CeleritiDepositsAPI/services/v1/depositAccounts/{accountNbr}/transactions':
    post:
      tags: 
       - Payments    
      summary: |
        Post deposit transactions.
      description: >
        The post deposit transaction function posts the transaction to a
        checking or savings account. The deposit and withdrawal transactions can
        be posted to an account depending on the transaction code provided in
        the input parameters. Transaction posted can increase or decrease the
        balance for an account.
      operationId: postDepositTransactions
      parameters:
        - name: transaction
          in: body
          description: Transaction details for posting a transaction to an account.
          schema:
            $ref: '#/definitions/transaction'
          required: true
        - name: companyNbr
          in: query
          description: Company Number.
          required: false
          type: integer
          format: int32
          minimum: 0
          maximum: 65535
        - name: accountType
          in: query
          description: >-
            Type of account.  
          required: true
          type: string
          format: byte
          maxLength: 3
        - name: accountNbr
          in: path
          description: Account Number.
          required: true
          type: integer
          format: int32
          minimum: 0
          maximum: 10000000000000000
      responses:
        '200':
          description: Transaction details of the transaction posted to an account.
          schema:
            $ref: '#/definitions/response'
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/xStatus'
        '404':
          description: Not found.
          schema:
            $ref: '#/definitions/xStatus'
        '422':
          description: >-
            Unprocessable entity. Will be used in case of business and
            validation error.
          schema:
            $ref: '#/definitions/xStatus'
        '500':
          description: Internal server error.
          schema:
            $ref: '#/definitions/xStatus'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/xStatus'
definitions:
  response:
    type: object
    properties:
      xStatus:
        $ref: '#/definitions/xStatus'
      companyNbr:
        type: integer
        format: int32
        description: The company number associated to the account. 
        minimum: 1
        maximum: 65535
      accountType:
        type: string
        format: byte
        description: The product code associated to the account.  
        maxLength: 3
      accountNbr:
        type: string
        format: byte
        description: The account number.
        maxLength: 23                
      transaction:
        $ref: '#/definitions/transaction'
  transaction:
    type: object
    required:
      - tranAmt
      - tranCode
    properties:
      tranId:
        type: string
        format: byte
        description: >-
          Unique identifier representing a specific transaction. This variable
          will not be part of request and will only come in response.
          Transaction id is generated when the transaction is posted to the
          account.
        maxLength: 100
      tranType:
        type: string
        format: byte
        description: >-
          Transaction type assigned to the transaction. This variable will not
          be part of request and will only come in response. 
        maxLength: 1
      createDt:
        type: string
        format: date YYYY-MM-DD
        description: Create date or effective date of the transaction.
      postDt:
        type: string
        format: date YYYY-MM-DD
        description: >-
          Date on which the transaction is posted. This variable will not be
          part of request and will only come in response
      currency:
        type: string
        format: byte
        description: >-
          Currency of the transaction amount. The currency will be defined as a
          3-character code. 
        maxLength: 3
      tranAmt:
        type: number
        format: float
        description: >-
          Transaction amount. Amount of the transaction posted. Maximum amount
          supported is 99999999999.99
        minimum: 0
        maximum: 99999999999.99
      tranDesc:
        type: string
        format: byte
        description: >-
          Brief description of the transaction. This is the default description
          of the transaction code with which the transaction is posted. Length
          is 47 characters alphanumeric. This variable will not be part of
          request and will only come in response when the Transaction Memo is
          not provided in request.
        maxLength: 47
      tranMemo:
        type: string
        format: byte
        description: >-
          Transaction Memo. User defined note or remarks for the transaction.
          This is the transaction description defined while posting the
          transaction. Length is 53 characters  alphanumeric.
        maxLength: 53
      tranCode:
        type: integer
        format: int32
        description: >-
          Transaction code. This defines the type of transaction to be posted.
        
        minimum: 0
        maximum: 9999
      checkNum:
        type: integer
        format: int32
        description: >-
          Check number of the instrument being used to post a transaction.Length
          is 10 characters numeric.
        minimum: 0
        maximum: 9999999999
      floatDays:
        type: integer
        format: int32
        description: >-
          Number of float days assigned to the float amount. Either floatDays or floatValueDt should be defined
          in request.
        minimum: 0
        maximum: 21
      floatAmt:
        type: number
        format: float
        description: >-
          Float amount. Amount which will remain in float till the check is
          realized. Maximum amount supported is 99999999999.99
        minimum: 0
        maximum: 99999999999.99
      floatValueDt:
        type: string
        format: date yyyy-mm-dd
        description: >-
          Float Value date. The Float value date is the check realization date
          of the instrument. This property name will not be part of response and
          will only come in request.
      overridePost:
        type: string
        format: byte
        description: >-
          Override Post. This field indicates if the transaction is to be posted
          or captured to be posted at a later date or time. If the value is
          defined as Save, the transaction is captured to be posted later and if
          the value is defined as Post, the transaction is posted.
        maxLength: 10
      ledgerBalance:
        type: number
        format: float
        description: >-
          Current ledger balance of the account after the transaction was
          posted. This variable will not be part of request and will only come
          in response. Maximum amount supported is 99999999999.99
        minimum: -99999999999.99
        maximum: 99999999999.99
      availableBalance:
        type: number
        format: float
        description: >-
          Current available balance of the account after the transaction was
          posted. This variable will not be part of request and will only come
          in response. Maximum amount supported is 99999999999.99
        minimum: -99999999999.99
        maximum: 99999999999.99
      sourceType:
        type: integer
        format: int32
        description: >-
          Source type for the transaction record. This variable will not be part
          of request and will only come in response. 
        maxLength: 2
      gliSource:
        type: string
        format: byte
        description: >-
          GLI source assigned to the transaction. This variable will not be part
          of request and will only come in response. 
        maxLength: 1
      traceId:
        type: string
        format: byte
        description: >-
          Trace identifier assigned to the transaction. It contains a system
          assigned sequence number. This variable will not be part of request
          and will only come in response.
        maxLength: 20
      traceIdMemo:
        type: string
        format: byte
        description: >-
          Trace Identifier Memo.This variable will not be part of request and
          will only come in response.
        maxLength: 10
      currentDataGroup:
        type: integer
        format: int32
        description: >-
          Data group identifier of the transaction. This variable will not be
          part of request and will only come in response.
        minimum: 0
        maximum: 9999
      transferCompanyNbr:
        type: integer
        format: int32
        description: >-
          In case of system generated fund transfers- if the transaction posted
          here is debit transaction, this is the Company identifier of the
          account being credited and if the transaction posted here is credit
          transaction, this is the Company identifier of the account being
          debited. This variable will not be part of request and will only come
          in response.
        minimum: 0
        maximum: 99999
      transferAccountType:
        type: string
        format: byte
        description: >-
          In case of system generated fund transfers- if the transaction posted
          here is debit transaction, this is the Account Type of the account
          being credited and if the transaction posted here is credit
          transaction, this is the Account type of the account being debited.
          This variable will not be part of request and will only come in
          response.
        maxLength: 3
      transferAccountNbr:
        type: integer
        format: int32
        description: >-
          In case of system generated fund transfers- if the transaction posted
          here is debit transaction, this is the Account number of the account
          being credited and if the transaction posted here is credit
          transaction, this is the Account number of the account being debited.
          This variable will not be part of request and will only come in
          response.
        maxLength: 23
  xStatus:
    type: object
    properties:
      statusMessage:
        type: string
        format: byte
        description: >-
          Status message. User readable message describing the status or error
          condition. This will display business message (Business message may be
          a successful or an error message in case it is coming from mainframe).
          If there is an OpenLegacy validation error, then “Invalid Error” would
          appear in this field.
        maxLength: 40
      severity:
        type: string
        format: byte
        description: >-
          Severity of the Status message. Severity will be defined as
          I-Information, E-error, W- warning and S-Severe.
        maxLength: 1
      statusCd:
        type: integer
        format: int32
        description: Status Code. Condition code assigned to the status message.
        minimum: 0
        maximum: 65535
      applicationCd:
        type: integer
        format: int32
        description: Application Code.
        minimum: 0
        maximum: 65535
      detailMessages:
        type: array
        items:
          $ref: '#/definitions/detailMessage'
  detailMessage:
    type: object
    properties:
      propertyName:
        type: string
        format: byte
        description: >-
          Identifies the property of the object that the error is related to.
          This is defined for validation errors.
        maxLength: 40
      messageDescription:
        type: string
        format: byte
        description: Message description.
        maxLength: 100
