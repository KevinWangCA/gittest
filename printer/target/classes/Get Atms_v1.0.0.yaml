swagger: '2.0'
info:
  version: 1.0.0
  title: Get ATMs
  description: >-
    Fetch nearby ATMs. 
    
    
    [Test Console](https://srvpocanz001.csc-fsg.com/CeleritiDepositsAPI/#!/Atm/getAtm) 
  contact:
    name: Karen Glimme
    url: 'http://www.celeritift.com'
    email: CFTSupportAPIs@celeritift.com
  license:
    name: © 2017 CeleritiFinTech, an HCL DXC Technology Company. All rights reserved
    url: 'http://celeritift.com/'
host: srvpocanz001.csc-fsg.com
tags:
  - name: Misc
    description: Get ATM's and branches
schemes:
  - https
produces:
  - application/json
paths:
  /CeleritiDepositsAPI/services/v1/atms:
    get:
      tags: 
       - Misc    
      summary: |
        Fetch the list of nearby ATMs for searched location.
      description: >
        This API requests will return detailed ATM information based on the
        criteria

        provided. This resource allows an application to retrieve all the
        details of all the ATMs near a given area. The key features of this API
        are

        -Finding ATMs based on latitude and longitude depending on the search
        radius

        -Find ATMs based on Postal Code
      operationId: GetATMs
      parameters:
        - name: postalCode
          in: query
          description: >-
            The postal code of location where we are looking for branch. User
            must provide value for either Postal code or latitude and longitude.
            Both will not come at a time.
          required: false
          type: integer
          format: int32
          minimum: 0
          maximum: 999999999      
        - name: distance
          in: query
          description: Search radius in which to locate the ATMs
          required: false
          type: number
          format: float
        - name: distanceUnit
          in: query
          description: 'Distance unit of search radius. Possible values - miles, km'
          required: false
          type: string
          format: byte
        - name: latitude
          in: query
          description: >-
            Latitude of location where we are looking for an ATM. If latitude is
            provided, longitude is required.
          required: false
          type: number
          format: float
          minimum: -90
          maximum: 90
        - name: longitude
          in: query
          description: >-
            Longitude of location where we are looking for an ATM. If Longitude
            is provided, latitude is required.
          required: false
          type: number
          format: float
          minimum: -180
          maximum: 180
      responses:
        '200':
          description: ATM list for a given location within a search radius
          schema:
            $ref: '#/definitions/atmList'
        '400':
          description: Bad request.
          schema:
            $ref: '#/definitions/xStatus'
        '404':
          description: Not found.
          schema:
            $ref: '#/definitions/xStatus'
        '422':
          description: >-
            Unprocessable entity. Will be used in case of business and
            validation error.
          schema:
            $ref: '#/definitions/xStatus'
        '500':
          description: Internal server error.
          schema:
            $ref: '#/definitions/xStatus'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/xStatus'
definitions:
  atmList:
    type: object
    properties:
      xStatus:
        $ref: '#/definitions/xStatus'
      atm:
        type: array
        items:
          properties:
            name:
              type: string
              format: byte
              description: Name of the ATM.
              maxLength: 40
            addressLine1:
              type: string
              format: byte
              description: First line of the ATM address.
              maxLength: 40
            addressLine2:
              type: string
              format: byte
              description: Second line of the ATM address.
              maxLength: 40
            city:
              type: string
              format: byte
              description: City where the ATM is located.
              maxLength: 25
            state:
              type: string
              format: byte
              description: State where the ATM is located.
              maxLength: 15
            postalCode:
              type: integer
              format: int32
              description: The postal code of the area where Atm is located.
              minimum: 0
              maximum: 999999999
            distance:
              type: number
              format: float
              description: Distance of the ATM from the searched location.
            distanceUnit:
              type: string
              format: byte
              description: 'Distance unit of search radius, Possible values - miles, km.'
            featuredServices:
              type: string
              format: byte
              description: Other featured services available at the ATM.
              maxLength: 268
  xStatus:
    type: object
    properties:
      statusMessage:
        type: string
        format: byte
        description: >-
          Status message. User readable message describing the status or error
          condition. This will display business message (Business message may be
          a successful or an error message in case it is coming from mainframe).
          If there is an OpenLegacy validation error, then “Invalid Error” would
          appear in this field.
        maxLength: 40
      severity:
        type: string
        format: byte
        description: >-
          Severity of the Status message. Severity will be defined as
          I-Information, E-error, W- warning and S-Severe.
        maxLength: 1
      statusCd:
        type: integer
        format: int32
        description: Status Code. Condition code assigned to the status message.
        minimum: 0
        maximum: 65535
      applicationCd:
        type: integer
        format: int32
        description: Application Code.
        minimum: 0
        maximum: 65535
      detailMessages:
        type: array
        items:
          $ref: '#/definitions/detailMessage'
  detailMessage:
    type: object
    properties:
      propertyName:
        type: string
        format: byte
        description: >-
          Identifies the property of the object that the error is related to.
          This is defined for validation errors.
        maxLength: 40
      messageDescription:
        type: string
        format: byte
        description: Message description.
        maxLength: 100
