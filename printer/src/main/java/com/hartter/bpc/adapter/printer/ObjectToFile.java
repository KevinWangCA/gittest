package com.hartter.bpc.adapter.printer;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.xml.sax.SAXException;

import io.swagger.parser.SwaggerParser;
import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.RefModel;
import io.swagger.models.Swagger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import io.swagger.parser.SwaggerParser;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ByteArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.Response;
//import io.swagger.models.Path;
import io.swagger.models.Model;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.refs.RefType;

import java.io.Serializable;  
import java.util.Date;

public class ObjectToFile {
	
	
	 public ObjectToFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public static  void createFunction(String rcPath1, String restMethod1, Restrepository.Restservice restservice1,io.swagger.models.Path sPath1,Swagger swagger1){
		Restrepository.Restservice.Function function = ObjectFactory.createRestrepositoryRestserviceFunction();
		Restrepository.Restservice.Function.Restcall restcall = ObjectFactory.createRestrepositoryRestserviceFunctionRestcall();
		Restrepository.Restservice.Function.Restcall.Httpinfo httpinfo = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfo();
		Restrepository.Restservice.Function.Restcall.Httpinfo.Path hPath = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoPath();
		Restrepository.Restservice.Function.Restcall.Httpinfo.Query hQuery = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoQuery();
		Restrepository.Restservice.Function.Restcall.Httpinfo.Body hBody = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoBody();

    	Operation getMethod = null;
    	switch(restMethod1){  	
    	case "POST":getMethod = sPath1.getPost();   				
    				break;
    	case "GET": getMethod = sPath1.getGet();
					break;
    	case "PUT": getMethod = sPath1.getPut();
    				break;
    	case "DELETE": getMethod = sPath1.getDelete();
    	            break;
    	}
    	
    	List<Parameter>lp = getMethod.getParameters();
    	
    	for (int i =0; i<lp.size(); i++){
		//restrepository/restservice/function/restcall/httpinfo/path,query/parameter(s) <-path.getGet.getParameters.parameter 5(6)th layer
		 Parameter p = lp.get(i);
		 if (p.getIn()=="path"){
			 Restrepository.Restservice.Function.Restcall.Httpinfo.Path.Param pParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoPathParam();
			 pParam.setRestname(p.getName());
			 pParam.setEainame(" ");
			 hPath.setParam(pParam);
			 httpinfo.setPath(hPath);
		 }else if(p.getIn()=="query"){
			 Restrepository.Restservice.Function.Restcall.Httpinfo.Query.Param qParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoQueryParam();
			 qParam.setRestname(p.getName());
			 qParam.setEainame(" ");
			 hQuery.getParam().add(qParam);
			 httpinfo.setQuery(hQuery);
		 }else if (p.getIn()=="body"){  			 
			 BodyParameter bp = (BodyParameter) p;
			 RefModel rPro1 =(RefModel) bp.getSchema();   
		     String simpleRefs = rPro1.getSimpleRef();
		     
   		    Model model = swagger1.getDefinitions().get(simpleRefs);
  	        Map<String, Property> mp = (Map<String, Property>)model.getProperties();
  	        for (String mpKey : mp.keySet() ){
  	   			 Restrepository.Restservice.Function.Restcall.Httpinfo.Body.Param bParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoBodyParam();
  	   			 bParam.setRestname(mpKey);
		  	   		if (mp.get(mpKey).getClass().equals(ObjectProperty.class)) {
						ObjectProperty oPP1 = (ObjectProperty)mp.get(mpKey);						
						Map<String, Property> mJP = (Map<String, Property>) oPP1.getProperties();
						for (String jpKey : mJP.keySet()){
			  	   			 Restrepository.Restservice.Function.Restcall.Httpinfo.Body.Param.Param1 bParamParam1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoBodyParamParam1();
			  	   			 bParamParam1.setRestname(jpKey);
			  	   			 bParamParam1.setEainame("   ");
			  	   			 bParam.getParam().add(bParamParam1);
						}
		  	   		}	 
	   			 bParam.setEainame("   ");
  	   			 hBody.getParam().add(bParam);
  	   			 httpinfo.setBody(hBody);
  	        }
		 }
    	}

	   	//restrepository/restservice/function/restcall/httpinfo/header  fixed content 5th layer
		Restrepository.Restservice.Function.Restcall.Httpinfo.Header hHeader = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoHeader();
		Restrepository.Restservice.Function.Restcall.Httpinfo.Header.Param hParam1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoHeaderParam();
		hParam1.setRestname("Accept");
		hParam1.setEainame("httpheader.accept");
		hHeader.getParam().add(hParam1);
		Restrepository.Restservice.Function.Restcall.Httpinfo.Header.Param hParam2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoHeaderParam();
		hParam2.setRestname("Authorization");
		hParam2.setEainame("httpheader.authorization");
		hHeader.getParam().add(hParam2);
	   	  	
	   	httpinfo.setHeader(hHeader);
	   	restcall.setHttpinfo(httpinfo);
	   	
	   	//restrepository/restservice/function/restcall/request  fixed content  4th layer 
	    Restrepository.Restservice.Function.Restcall.Request rRequest = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallRequest();
	    rRequest.setContenttyp("application/json/utf8");
	    restcall.setRequest(rRequest);
		    
	    //restrepository/restservice/function/restcall/response  fixed content  4th layer 
	    Restrepository.Restservice.Function.Restcall.Response rResponse = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponse();
	    rResponse.setContenttyp("application/json/utf8");
		    	    	    
	    //insert context to restrepository/restservice/function/restcall/response based on status"200"    
	    Map<String,Response> lr = getMethod.getResponses();
	    Response r = lr.get("200");
	    RefProperty rPro =(RefProperty) r.getSchema();             	    
	    String simpleRef = rPro.getSimpleRef();//depositAccounts
		   
	    //restrepository/restservice/function/restcall/response/json  empty attribute  5th layer
	    Restrepository.Restservice.Function.Restcall.Response.Json rJson = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJson();
	    
		Model model = swagger1.getDefinitions().get(simpleRef);
	    Map<String, Property> mp = (Map<String, Property>)model.getProperties();
		    for (String mpKey : mp.keySet() ){
		    	//restrepository/restservice/function/restcall/response/json/json:param  empty   6th layer
		    	Restrepository.Restservice.Function.Restcall.Response.Json.Par par = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonPar();
		    	par.setJsonname(mpKey);
		    	par.setEainame("   ");
		    	
		    	//if par has para, add para to par then add par to rJson
		    	if (mp.get(mpKey).getClass().equals(RefProperty.class)){
		    		RefProperty rPro1 = (RefProperty)mp.get(mpKey);
		    		String simpleRef1 = rPro1.getSimpleRef();
		    		Model mode2 = swagger1.getDefinitions().get(simpleRef1);
				    Map<String, Property> mp1 = (Map<String, Property>)mode2.getProperties();
				    for (String mpKey1 : mp1.keySet() ){
				    	Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
				    	para.setJsonname(mpKey1);
				    	para.setEainame("   ");
				    	
				    	//if para has param2, add param2 to para then add para to par
				    	if(mp1.get(mpKey1).getClass().equals(ObjectProperty.class)){
				    		ObjectProperty oPP = (ObjectProperty)mp1.get(mpKey1);
				    		Map<String, Property> mJP = (Map<String, Property>) oPP.getProperties();
				    		for (String jpKey1 : mJP.keySet()){
		    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param2 param2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam2();
		    	    			param2.setJsonname(jpKey1);
		    	    			param2.setEainame("   ");
		    	    			
		    	    			para.getParam2().add(param2);
				    		}
				    	}
				    	
				    	if(mp1.get(mpKey1).getClass().equals(ArrayProperty.class)){
				    		ArrayProperty oPP = (ArrayProperty)mp1.get(mpKey1);
				    		RefProperty oPPitem = (RefProperty)oPP.getItems();
				    		String oPPReference = oPPitem.getSimpleRef();
				    		Model model3 = swagger1.getDefinitions().get(oPPReference);
						    Map<String, Property> mp2 = (Map<String, Property>)model3.getProperties();
						    for (String mpKey2 : mp2.keySet() ){
		    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param2 param2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam2();
		    	    			param2.setJsonname(mpKey2);
		    	    			param2.setEainame("   ");
		    	    			
		    	    			para.getParam2().add(param2);
						    }
				    		
				    	} 
				    	par.getPara().add(para);
				    }
		    		
		    	}else if (mp.get(mpKey).getClass().equals(ArrayProperty.class)) {
		    		
		    		ArrayProperty oPP = (ArrayProperty)mp.get(mpKey);
		    		if (oPP.getItems().getClass().equals(RefProperty.class)){
			    		RefProperty oPPitem = (RefProperty)oPP.getItems();
			    		String oPPReference = oPPitem.getSimpleRef();
			    		Model model3 = swagger1.getDefinitions().get(oPPReference);
					    Map<String, Property> mp2 = (Map<String, Property>)model3.getProperties();
					    for (String mpKey2 : mp2.keySet() ){
					    	Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
					    	para1.setJsonname(mpKey2);
					    	para1.setEainame("   ");
					    	
						    	if(mp2.get(mpKey2).getClass().equals(ObjectProperty.class)){
						    		ObjectProperty oPP2 = (ObjectProperty)mp2.get(mpKey2);
						    		Map<String, Property> mJP = (Map<String, Property>) oPP2.getProperties();
						    		for (String jpKey1 : mJP.keySet()){
				    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param2 param2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam2();
				    	    			param2.setJsonname(jpKey1);
				    	    			param2.setEainame("   ");
				    	    			
				    	    			para1.getParam2().add(param2);
						    		}
						    	}
					    	par.getPara().add(para1);
					    }
		    		}else if (oPP.getItems().getClass().equals(ObjectProperty.class)){
		    			ObjectProperty oPP4 = (ObjectProperty)oPP.getItems();
						Map<String, Property> mOPP1 = (Map<String, Property>)oPP4.getProperties();
	    	    		for (String mOPPKey : mOPP1.keySet()) {
					    	Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
	    	    			if (mOPP1.get(mOPPKey).getClass().equals(ArrayProperty.class)){
	    	    				ArrayProperty oPP1 = (ArrayProperty)mOPP1.get(mOPPKey);
	    	    				ObjectProperty oPP11 = (ObjectProperty)oPP1.getItems();
	    	    				Map<String, Property> mOPP = (Map<String, Property>)oPP11.getProperties();
	    	    	    		for (String mOPPKey1 : mOPP.keySet()) {
			    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param2 param4 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam2();
			    	    			param4.setJsonname(mOPPKey1);
			    	    			param4.setEainame("   ");
			    	    			para2.getParam2().add(param4);
	    	    	    		}
	    	    				
	    	    			}
					    	para2.setEainame("   ");
	    	    			para2.setJsonname(mOPPKey);
	    	    			par.getPara().add(para2);
	    	    			
	    	    		}
		    		}
		    	}else if (mp.get(mpKey).getClass().equals(ObjectProperty.class)) {
		    		ObjectProperty oPP3 = (ObjectProperty)mp.get(mpKey);
					Map<String, Property> mOPP = (Map<String, Property>)oPP3.getProperties();
    	    		for (String mOPPKey : mOPP.keySet()) {
    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para2 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
    	    			para2.setEainame("   ");
    	    			para2.setJsonname(mOPPKey);
    	    			par.getPara().add(para2);
    	    			
    	    		}
		    	}
		    	rJson.getPar().add(par);
	
		    }
		    rResponse.setJson(rJson);
		    restcall.setResponse(rResponse);
		    
		    restcall.setPath(rcPath1);
		    restcall.setMethod(restMethod1);
	   	
		    function.setName("  ");
		    function.setRestcall(restcall);
		    restservice1.getFunction().add(function);	
	 
	}
	
	public void convertFileToObject(String filename)throws JAXBException, IOException, SAXException{
		
		//reading YAML file and creating the object holding YAML data
		final SwaggerParser parser = new SwaggerParser();
        final Swagger swagger = parser.read(filename);
    	   	
    	//Createing object for xml and binding data to this object
    	// Create Context
        final JAXBContext context = JAXBContext.newInstance(Restrepository.class);
        // Java to XML -> Marshaller needed
        final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);        
               
        // Using ObjectFactory and creating the object for xml
        ObjectFactory objectFactory = new ObjectFactory();
        
        //create restrepository 1st layer
        Restrepository restrepository = objectFactory.createRestrepository();
        
        restrepository.setxmlns("http://hartter.com/vsop/eai/eairestservice/repository");
        restrepository.setxmlnsjson("http://hartter.com/vsop/eai/eaiserviceutil/basejson/repository");
	    
        //create restrepository/restservice <-- paths 2nd layer               
        LinkedHashMap<String, io.swagger.models.Path> mm = (LinkedHashMap<String, io.swagger.models.Path>)swagger.getPaths();
        for (String key : mm.keySet()){
        	   
        	    Restrepository.Restservice restservice = objectFactory.createRestrepositoryRestservice();
        		String keyPath = key;
        		String[] nameParts = keyPath.split("/");
        		String rsName = nameParts[1];
        		String rsPath = "/" + nameParts[1] + "/" + nameParts[2];
        		String rcPath = keyPath.replace(rsPath,"");
        		
        		restservice.setName(rsName);
            	restservice.setPath(rsPath);
            	
        	    io.swagger.models.Path sPath = mm.get(key);          
        	    LinkedHashMap<HttpMethod,Operation> httpMap = (LinkedHashMap<HttpMethod,Operation>) sPath.getOperationMap();
        	    for (HttpMethod httpMethod : httpMap.keySet()) {
        	    	String restMethod = httpMethod.name();
        	    	createFunction(rcPath,restMethod,restservice,sPath,swagger);  
        	    }
        	    
        		restrepository.getRestservice().add(restservice);
        }
        
        // Marshalling
        final Path target = Paths.get("Create Checking Account_v1.0.0.yaml.xml");
        try (final OutputStream out = Files.newOutputStream(target)) {
            marshaller.marshal(restrepository, out);
        }
            
	}
	
    public static void main(String[] args) throws JAXBException, IOException, SAXException {
    	ObjectToFile objectToFile = new ObjectToFile();
    	objectToFile.convertFileToObject("Create Checking Account_v1.0.0.yaml");
    }
    
}