package test;

import java.io.FileNotFoundException;  
import java.io.FileReader;  
import java.io.FileWriter;  
import java.io.IOException;  
import java.io.StringReader;  
import java.io.StringWriter;  
  
import javax.xml.bind.JAXBContext;  
import javax.xml.bind.JAXBException;  
import javax.xml.bind.Marshaller;  
import javax.xml.bind.Unmarshaller;  

public class XMLUtil {

	/**
	 * To convert Object to XML (String)
	 * 
	 * @param obj
	 * @return
	 */
	
	public static String convertToXml(Object obj){
		
		StringWriter sw = new StringWriter();
		try {
			
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			
			Marshaller marshaller = extracted(context);
			
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.class);
			marshaller.marshal(obj, sw);
			
		}catch (JAXBException e){
			e.printStackTrace();
		}
		
		return sw.toString();
	}

	private static Marshaller extracted(JAXBContext context)
			throws JAXBException {
		return context.createMarshaller();
	}
	
	/**
	 * To convert object to XML
	 * 
	 * @param obj
	 * @param path
	 * @return
	 */
	
	public static void convertToXml(Object obj, String path){
		try{
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			
			Marshaller marshaller = extracted(context);
			
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			FileWriter fw = null;
			try{
				fw = new FileWriter(path);
			}catch (IOException e){
				e.printStackTrace();
			}
			marshaller.marshal(obj, fw);
		 }catch (JAXBException e){
		 	e.printStackTrace();
		 }
	}
	
}
