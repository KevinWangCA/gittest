package test;

import javax.xml.bind.annotation.*;

@SuppressWarnings("restriction")
@XmlRootElement(name="Customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
   @XmlElement(name="first-name")
   private String firstName;
 
   @XmlElement(name="last-name")
   private String lastName;
 
   public Customer (){
	   
   }
}
